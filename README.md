# W1UrlScheme

Библиотека, с помощь которой можно легко передавать платежную информацию из приложения интернет магазина в приложение банка и получать обратно номер транзакции и статус платежа. 

## Требования

- iOS >= 7.0

### Установка

- ##### Ручная
Перенести папку W1UrlScheme_Lib с содержимым в свой проект

- #### Установка с помощью CocoaPods
[CocoaPods](http://cocoapods.org/) является менеджером зависимостей для Objective-C, который автоматизирует и упрощает процесс использования сторонних библиотек как W1UrlScheme в вашем проекте.
#### Podfile
```
platform :ios, '7.0'
pod 'W1UrlScheme', :git => 'https://bespalown@bitbucket.org/bespalown/w1urlscheme.git'
```

## Архитектура
---
```
W1UrlScheme.h
```
### Главный файл проекта, импортируем его для работы с библиотекой 

---

```
W1UrlSchemeConstant.h
```
### Файл с константами:

1. список идентификаторов валюты, согласно ISO 4217
2. список возможных статусов транзакций
3. список доступных провайдеров


---
### Для отправки информации платежа из приложения магазина в приложение банка

```
W1UrlSchemeRequest.h
```
##### Создаем параметры платежа для отправки в приложение банка. Состоит из 2 методов.

```
/**
 *
 *  @param seller           номер кошелька мерчанта
 *  @param amount           сумма платежа
 *  @param currency         идентификатор валюты, согласно ISO 4217
 *  @param orderDescription описание
 *  @param returnUrlScheme  название URL Scheme для обратного возврата в приложение
 *
 */
- (id)initWithSeller:(long long )seller
              amount:(CGFloat )amount
            currency:(W1CurrencyCode )currency
         description:(NSString *)orderDescription
     returnUrlScheme:(NSString *)returnUrlScheme
     paymentProvider:(W1PaymentProvider )paymentProvider;

/**
 *  Отправляем данные для оплаты в стороннее приложение
 *  Если вернется nil то запрос на оплату ушел, иначе смотрим ошибку
 */
- (void)sendRequest;
```


```
W1UrlSchemeRequestAnswer.h
```
##### Получает ответ от приложения банка
Создается в методе делегата application:handleOpenURL и заполняет все property
 
```
- (id)initWithHandleOpenURL:(NSURL *)url;
```

---
### Создается в приложении на стороне банка

```
W1UrlSchemeResponse.h
```
##### Получает платежную информацию, которую отправил магазин

Создается в методе делегата application:handleOpenURL и заполняет все property

```
- (id)initWithHandleOpenURL:(NSURL *)url;
```

Для возврата обратно в приложение магазина вызывается метод с параметрами

```
/**
 *  @param transactionId            //номер транзакции платежа
 *  @param paymentTransactionStatus //статус платежа
 */
- (void)sendResponseWithTransactionId:(NSString *)transactionId
          andPaymentTransactionStatus:(W1PaymentTransactionStatus )paymentTransactionStatus
                             delegate:(id<w1UrlSchemeDelegate>)delegate;
 ```
 
 ---
                             
```
W1UrlSchemeDelegate.h
```

Делегат возращает ошибки при вызове sendResponseWithTransactionId:andPaymentTransactionStatus:delegate
Если ошибок нет то в приложение магазина вернется номер транзакции и статус платежа.

```
- (void)w1UrlSchemeException:(NSException *)exception;
```