Pod::Spec.new do |s|
  s.name                  = "W1UrlScheme"
  s.version               = "0.0.4"
  s.summary               = "Library for sending and receiving your payment information in third-party applications"
  s.description           = <<-DESC
                          Library for sending and receiving your payment information in third-party applications
                          DESC
  s.homepage              = "http://www.walletone.com/ru/merchant/"
  s.license               = { :type => 'MIT', :file => 'LICENSE' }
  s.author                = { "Viktor Bespalov" => "bespalown@gmail.com" }
  s.platform              = :ios, '7.0'
  s.source                = { :git => 'https://bespalown@bitbucket.org/bespalown/w1urlscheme.git', :tag => s.version.to_s }
  s.source_files          = 'W1UrlScheme_Lib/headers/*.h'
  s.public_header_files   = 'W1UrlScheme_Lib/headers/*.h'
  s.header_dir            = 'W1UrlScheme_Lib/headers'
  s.preserve_path         = 'W1UrlScheme_Lib/lib/libw1urlscheme.a'
  s.vendored_library      = 'W1UrlScheme_Lib/lib/libw1urlscheme.a'
  s.library               = 'w1urlscheme'
  s.requires_arc          = true
end
